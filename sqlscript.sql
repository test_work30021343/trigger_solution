-- сам триггер, который отрабатывает до вставки в таблицу events
create
or replace trigger check_time_difference
    before insert
    on events
    for each row
execute function check_time_difference_func();

-- функция, которая считает разницу по времени(в секундах) между последней записью и новой
create or replace function check_time_difference_func()
    returns trigger as
$$
declare
diff_interval   interval;
    last_value      timestamp;
    diff_in_seconds bigint;
    response        text;
BEGIN
    select ts into last_value from events order by ts desc limit 1;
    diff_interval = new.ts - last_value;
    select(extract(epoch from diff_interval)) into diff_in_seconds;
    if (diff_in_seconds > 2) then
--         логика по по отправке запроса на сервак
        response = post('https://notification.test/v1/alert', '{
          "message": "В прошлую секунду какой то из сервисов не прислал сообщение"
        }');
    end if;
    raise notice 'diff second: %', diff_in_seconds;
return new;
end
$$ language plpgsql;


--функция для отправки запроса на web-server
CREATE
OR REPLACE FUNCTION post(url TEXT, request JSON) RETURNS TEXT
    LANGUAGE SQL AS
$BODY$
WITH s AS (SELECT curl_easy_reset(),
                  curl_easy_setopt_postfields(convert_to(request::TEXT, 'utf-8')),
                  curl_easy_setopt_url(url),
                  curl_header_append('Content-Type', 'application/json; charset=utf-8'),
                  curl_easy_perform(),
                  curl_easy_getinfo_data_in())
SELECT convert_from(curl_easy_getinfo_data_in, 'utf-8')
FROM s;
$BODY$;

--сама таблица ивентов
CREATE TABLE events
(
    id           UUID,
    ts           TIMESTAMP,
    service_name VARCHAR(255),
    msg          VARCHAR(255),
    PRIMARY KEY (id)
);